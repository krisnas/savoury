<?php include "koneksi.php" ?>
<?php 

session_start();

if (!isset($_SESSION['username'])) {
  header("Location: login.php");
}

?>
<!doctype html>
<html lang="en">

<head>

  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- page icon -->
  <link rel="shorcut icon" href="assets/img/logo.png"> <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="node_modules/bootstrap/dist/css/bootstrap.min.css">

  <!-- OwlCarouse CDN -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css" />
  <link rel="stylesheet"
    href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css" />


  <!-- FontAwesome CDN -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" />

  <!-- fonts custom -->
  <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600&display=swap" rel="stylesheet">

  <!-- my css -->
  <link rel="stylesheet" href="assets/css/style.css">
  <!-- bootstrap cdn -->
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet"
    integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">

  <script src="https://unpkg.com/scrollreveal@4.0.0/dist/scrollreveal.min.js"></script>

  <title>Savoury - your needed </title>
  <!-- chat bot -->
  <script
    type="text/javascript">window.$crisp = []; window.CRISP_WEBSITE_ID = "c8e9e32d-773e-4a8c-a220-0e2ac7efeb60"; (function () { d = document; s = d.createElement("script"); s.src = "https://client.crisp.chat/l.js"; s.async = 1; d.getElementsByTagName("head")[0].appendChild(s); })();</script>
</head>

<body class="bg-light">

  <!-- header section -->
  <nav class="navbar navbar-expand-lg navbar-light bg-light shadow-sm fixed-top">
    <div class="container-lg">
      <a class="navbar-brand" href="#">sav<span>our</span>y.</a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav"
        aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav ms-auto">
          <li class="nav-item active">
            <a class="nav-link" href="#home">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#about">About</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#services">Services</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#menu">Menu</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#contact">Contact Us</a>
          </li>
          
          <?php
          if(ISSET($_SESSION['users'])){ ?>
          <li class="nav-item">
            <a class="nav-link" href="login.php">Login</a>
          </li>
          <?php } else {?>
          <li class="nav-item">
            <a class="nav-link" href="logout.php"><?php echo $_SESSION['username']; ?></a>
          </li>
          
          <!-- <div class="cart-icon my-auto bg-red social" data-bs-toggle="modal" data-bs-target="#cartModal"> -->
          <a href="cart.php" class="cart-icon my-auto bg-red social"> 
            <i class="fas fa-shopping-cart"></i></a>
          </div>
          <?php } ?>
        </ul>
      </div>
    </div>
  </nav>
  <!-- header end -->

 

  <!-- home section -->
  <section class="home py-5" id="home">
    <div id="carouselExampleInterval" class="carousel slide" data-bs-ride="carousel">
      <div class="container-lg">
        <div class="row min-vh-100 align-items-center align-content-center">
          <div class="col-md-6 mt-5 mt-md-0">
            <div class="home-img text-center">
              <div class="carousel-inner">
                <div class="col-lg-8">
                  <div class="carousel-item active" data-bs-interval="2000">
                    <img src="assets/img/home.png" alt="home img" width="500">
                  </div>
                  <div class="carousel-item" data-bs-interval="2000">
                    <img src="assets/img/water.png" alt="home img" width="500">
                  </div>
                  <div class="carousel-item" data-bs-interval="2000">
                    <img src="assets/img/orange.png" alt="home img" width="500">
                  </div>
                </div>
              </div>
            </div>
          </div>


          <div class="col-md-6 mt-5 mt-md-0 order-md-first">
            <div class="home-text">
              <p class="fs-1 fw-bold mb-1 "><span>our</span> Delicious m<span>é</span>nu</p>
              <h1 class="fs-4">Try the best menu that we have.</h1>
              <a href="#menu" class="btn btn-danger px-3 mt-3 btn-home">View Menu</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- home section end -->

  <!-- about section -->

  <section class="about py-5" id="about">
    <div class="container-lg py-4">
      <div class="row py-3 align-items-center align-content-center mt-5">
        <div class="col-md-6 mt-5 mt-md-0">
          <div class="about-img text-center">
            <img src="assets/img/Capture.PNG" alt="home img" width="400">
          </div>
        </div>
        <div class="col-md-6 mt-5 mt-md-0">
          <div class="about-text">
            <h5 class="fw-bold mb-3 text-danger">About Us</h5>
            <h1 class="fs-1 fw-bold">try <span>our</span> delicious<br>and high quality food.</h1>
            <br>
            <p class="fs-5">We cook the best food in the entire city,
              with excellent customer service, the best
              meals and at the best price, visit us.
            </p>
            <a href="#contact" class="btn btn-danger px-3 mt-3 btn-about">Contact Us</a>
          </div>
        </div>
      </div>
    </div>
    </div>
  </section>
  <!-- about section end -->

  <!-- service section -->
  <section class="services py-5" id="services">
    <div class="container-lg py-4">
      <div class="row justify-content-center">
        <div class="col-lg-8">
          <div class="section-title text-center">
            <h5 class=" fw-bold mb-1 text-danger">Offering</h5>
            <h2 class="fw-bold mb-5 fs-1"><span>our</span> Amazing Services</h2>
          </div>
        </div>
      </div>
      <div class="row text-center">
        <div class="col-md-6 col-lg-4 mb-4">
          <div class="service-item p-4 bg-white rounded showcase-kiri">
            <div class="icon my-3 text-danger fs-1">
              <i class="fas fa-pizza-slice fa-lg"></i>
            </div>
            <h3 class="fs-5 py-2 fw-bold">Fast Food</h3>
            <p class="text-muted fs-5">We've served <span>our</span> customers for many years with our best service and
              delicious
              food.
            </p>
          </div>
        </div>
        <div class="col-md-6 col-lg-4 mb-4">
          <div class="service-item p-4 bg-white rounded showcase-tengah">
            <div class="icon my-3 text-danger fs-1">
              <i class="fas fa-truck fa-lg"></i>
            </div>
            <h3 class="fs-5 py-2 fw-bold">Delivery</h3>
            <p class="text-muted fs-5">We've served <span>our</span> customers for many years with our best service and
              delicious
              food.
            </p>
          </div>
        </div>
        <div class="col-md-6 col-lg-4 mb-4">
          <div class="service-item p-4 bg-white rounded showcase-kanan">
            <div class="icon my-3 text-danger fs-1">
              <i class="fas fa-thumbs-up fa-lg"></i>
            </div>
            <h3 class="fs-5 py-2 fw-bold">Excelent Food</h3>
            <p class="text-muted fs-5">We've served <span>our</span> customers for many years with our best service and
              delicious
              food.
            </p>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- service section end -->

  <!-- menu section -->
  <section class="menus py-5" id="menu">
    <div id="multi-item-example" class="carousel slide carousel-multi-item" data-ride="carousel">
      <div class="container">

        <div class="row justify-content-center">
          <div class="col-lg-8">
            <div class="section-title text-center">
              <h5 class=" fw-bold mb-1 text-danger mt-4">Special</h5>
              <h2 class="fw-bold mb-5 fs-1">Menu of the Week</h2>
            </div>
          </div>
        </div>

        <div class="carousel-inner" role="listbox">

          <div class="carousel-item active">
            <div class="col-md-3 p-4" style="float:left">
              <div class="menu-item p-1 bg-white rounded" data-bs-toggle="modal" data-bs-target="#modal-menu8">
                <img class="menu my-5 mx-4" src="assets/img/plate8.png" alt="Card image cap">
                <div class="card-body">
                  <b class="card-title" id="card-title">Mae Pranom Tomyam Thai</b><br>
                  <p class="card-desc">Delicious Dish</p>
                  <p class="card-price">Rp. 999.999</p>
                  <a href="#" class="btn btn-danger rounded-pill"><i class="fas fa-shopping-cart"></i></a>
                </div>
              </div>
            </div>

            <div class="col-md-3 p-4" style="float:left">
              <div class="menu-item p-1 bg-white rounded" data-bs-toggle="modal" data-bs-target="#modal-wings">
                <img class="menu my-5 mx-4" src="assets/img/wings.png" alt="Card image cap">
                <div class="card-body">
                  <b class="card-title" id="card-title">Chicken Karaage</b><br>
                  <p class="card-desc">Delicious Dish</p>
                  <p class="card-price">Rp. 999.999</p>
                  <a href="#" class="btn btn-danger rounded-pill"><i class="fas fa-shopping-cart"></i></a>
                </div>
              </div>
            </div>

            <div class="col-md-3 p-4 " style="float:left">
              <div class="menu-item p-1 bg-white rounded" data-bs-toggle="modal" data-bs-target="#modal-plate7">
                <img class="menu my-5 mx-4 " src="assets/img/plate7.png" alt="Card image cap">
                <div class="card-body">
                  <b class="card-title" id="card-title">Kare Burosu Ramen Wagama</b><br>
                  <p class="card-desc">Delicious Dish</p>
                  <p class="card-price">Rp. 999.999</p>
                  <a href="#" class="btn btn-danger rounded-pill"><i class="fas fa-shopping-cart"></i></a>
                </div>
              </div>
            </div>

            <div class="col-md-3 p-4" style="float:left">
              <div class="menu-item p-1 bg-white rounded" data-bs-toggle="modal" data-bs-target="#modal-plate6">
                <img class="menu my-5 mx-4" src="assets/img/plate6.png" alt="Card image cap">
                <div class="card-body">
                  <b class="card-title" id="card-title">Chicken Green Curry</b><br>
                  <p class="card-desc">Delicious Dish</p>
                  <p class="card-price">Rp. 999.999</p>
                  <a href="#" class="btn btn-danger rounded-pill"><i class="fas fa-shopping-cart"></i></a>
                </div>
              </div>
            </div>

          </div>
          <!-- slide 2 -->
          <div class="carousel-item">
            <div class="col-md-3 p-4" style="float:left">
              <div class="menu-item p-1 bg-white rounded" data-bs-toggle="modal" data-bs-target="#modal-plate4">
                <img class="menu my-5 mx-4" src="assets/img/plate4.png" alt="Card image cap">
                <div class="card-body">
                  <b class="card-title" id="card-title">Spicy Asian Cuisine</b><br>
                  <p class="card-desc">Delicious Dish</p>
                  <p class="card-price">Rp. 999.999</p>
                  <a href="#" class="btn btn-danger rounded-pill"><i class="fas fa-shopping-cart"></i></a>
                </div>
              </div>
            </div>

            <div class="col-md-3 p-4" style="float:left">
              <div class="menu-item p-1 bg-white rounded" data-bs-toggle="modal" data-bs-target="#modal-plate3">
                <img class="menu my-5 mx-4" src="assets/img/plate3.png" alt="Card image cap">
                <div class="card-body">
                  <b class="card-title" id="card-title">Spinach Salad with Strawberry</b><br>
                  <p class="card-desc">Delicious Salad</p>
                  <p class="card-price">Rp. 999.999</p>
                  <a href="#" class="btn btn-danger rounded-pill"><i class="fas fa-shopping-cart"></i></a>
                </div>
              </div>
            </div>

            <div class="col-md-3 p-4" style="float:left">
              <div class="menu-item p-1 bg-white rounded" data-bs-toggle="modal" data-bs-target="#modal-plate2">
                <img class="menu my-5 mx-4" src="assets/img/plate2.png" alt="Card image cap">
                <div class="card-body">
                  <b class="card-title" id="card-title">Crudités Greek Salad</b><br>
                  <p class="card-desc">Delicious Salad</p>
                  <p class="card-price">Rp. 999.999</p>
                  <a href="#" class="btn btn-danger rounded-pill"><i class="fas fa-shopping-cart"></i></a>
                </div>
              </div>
            </div>

            <div class="col-md-3 p-4" style="float:left">
              <div class="menu-item p-1 bg-white rounded" data-bs-toggle="modal" data-bs-target="#modal-cinammons">
                <img class="menu my-5 mx-4" src="assets/img/cinammons-sticks 1.png" alt="Card image cap">
                <div class="card-body">
                  <b class="card-title" id="card-title">Cinammons Sticks</b><br>
                  <p class="card-desc">Delicious Snacks</p>
                  <p class="card-price">Rp. 999.999</p>
                  <a href="#" class="btn btn-danger rounded-pill"><i class="fas fa-shopping-cart"></i></a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="controls-top">
        <button class="carousel-control-prev" type="button" data-bs-target="#multi-item-example" data-bs-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true" style="filter: invert();"></span>
          <span class="visually-hidden">Previous</span>
        </button>
        <button class="carousel-control-next" type="button" data-bs-target="#multi-item-example" data-bs-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true" style="filter: invert();"></span>
          <span class="visually-hidden">Next</span>
        </button>
      </div>

    </div>
  </section>
  <!-- menu section end -->

  <!-- contact us section -->
  <section class="contact py-4" id="contact">
    <div class="container-lg">
      <div class="row">
        <div class="col-lg-8">
          <div class="section-title mt-5">
            <h2 class="fw-bold mb-3 fs-1">Contact Us</h2>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-5">
          <div class="contact-text">
            <div class="text fs-5 text-danger">
              <h5>Get in touch</h5>
            </div>
            <div class="text">
              <p class="fs-5">If you want to reserve a table in <span>our</span>
                restaurant, contact us and we will attend
                you quickly, with <span>our</span> 24/7 chat service.
              </p>
            </div>
          </div>
        </div>
        <div class="col-md-7">
          <h5>How Can We Help You?</h5>
          <div class="contact-form">
            <form>
              <div class="row">
                <div class="col-lg-6 mb-4">
                  <input type="text" placeholder="Name" class="form-control form-control-lg fs-6 border-0 shadow-sm">
                </div>
                <div class="col-lg-6 mb-4">
                  <input type="text" placeholder="Email" class="form-control form-control-lg fs-6 border-0 shadow-sm">
                </div>
              </div>
              <div class="row">
                <div class="col-lg-12 mb-4">
                  <input type="text" placeholder="Subject" class="form-control form-control-lg fs-6 border-0 shadow-sm">
                </div>
              </div>
              <div class="row">
                <div class="col-lg-12 mb-4">
                  <textarea rows="5" placeholder="Message"
                    class="form-control form-control form-control-lg fs-6 border-0 shadow-sm"></textarea>
                </div>
              </div>
              <div class="row">
                <div class="col-lg-12">
                  <button type="submit" class="btn btn-danger px-3">Send</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- contact us section end -->

  <!-- footer section -->
  <footer class="footer border-top py-4">
    <div class="container-lg">
      <div class="row">
        <div class="col-md-3 lg-3 col-xl-3 mx-auto mt-3">
          <h5 class="mb-4 fw-bold footer_text">sav<span>our</span>y.</h5>
          <ul class="list-unstylyed list-inline">
            <li class="list-inline-item">
              <a href="https://www.facebook.com/ksonezz" class="btn-floating btn-lg text-dark"><i
                  class="fab fa-facebook-f"></i></a>
            </li>
            <li class="list-inline-item">
              <a href="https://www.instagram.com/krsnsu" class="btn-floating btn-lg text-dark"><i
                  class="fab fa-instagram"></i></a>
            </li>
            <li class="list-inline-item">
              <a href="https://wa.me/6281234567890""
                class=" btn-floating btn-lg text-dark"><i class="fab fa-whatsapp"></i></a>
            </li>
          </ul>
        </div>
        <div class="col-md-2 col-lg-2 col-xl-2 mx-auto mt-3">
          <h5 class="text-uppercase mb-4 fw-bold">Services</h5>
          <p><a href="#" class="text-dark" style="text-decoration: none;">Delivery</a></p>
          <p><a href="#" class="text-dark" style="text-decoration: none;">Pricing</a></p>
          <p><a href="#" class="text-dark" style="text-decoration: none;">Fast Food</a></p>
          <p><a href="orderedmenu.php" class="text-dark" style="text-decoration: none;">Reserve your Spot</a></p>
        </div>
        <div class="col-md-3 col-lg-2 col-xl-2 mx-auto mt-3">
          <h5 class="text-uppercase mb-4 fw-bold">Information</h5>
          <p><a href="#" class="text-dark" style="text-decoration: none;">Event</a></p>
          <p><a href="#" class="text-dark" style="text-decoration: none;">Contact Us</a></p>
          <p><a href="#" class="text-dark" style="text-decoration: none;">Privacy Policy</a></p>
          <p><a href="#" class="text-dark" style="text-decoration: none;">Terms of Services</a></p>
        </div>
        <div class="col-md-4 col-lg-3 col-xl-3 mx-auto mt-3">
          <h5 class="text-uppercase mb-4 fw-bold">Address</h5>
          <p><i class="fas fa-map-marker-alt me-3"></i>Purwokerto - Jawa Tengah - Indonesia</p>
          <p><i class="fas fa-phone me-3"></i>234-234-234</p>
          <p><i class="far fa-envelope me-3"></i>savoury@mail.com</p>
        </div>
        <footer class="footer border-top py-1">
          <div class="container-lg">
            <div class="row">
              <div class="col-lg-12">
                <p class="m-0 text-center text-muted footer_text">&copy; 2021 sav<span>our</span>y.</p>
              </div>
            </div>
          </div>
        </footer>
      </div>
    </div>
  </footer>
  <!-- footer section end -->

  <!-- modal cart -->
  <!-- Modal -->
    <!-- <div class="modal fade" id="cartModal" tabindex="-1" aria-labelledby="exampleModalLabel2" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Your Order</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>

          <form action="simpan_cart.php" method="POST">
          <div class="modal-body">
            <div class="form-group">
              <label>Your Name</label>
              <input type="text" name="nama" class="form-control" placeholder="Your Name">
            </div>
            <div class="form-group">
              <label>Phone Number</label>
              <input type="number" name="no_telp" class="form-control" placeholder="Phone Number">
            </div>
            <div class="form-group">
              <label>Delivery / Pick Up</label>
              <input type="text" name="layanan" class="form-control" placeholder="Service">
            </div>
            <div class="form-group">
              <label>Your Address</label>
              <input type="text" name="alamat" class="form-control" placeholder="Your Address">
            </div>
            
          </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
            <button type="button" name="simpan_cart" class="btn btn-primary">Save Data</button>
          </div>
        </div>
      </div>
    </div> -->
  <!-- modal cart end -->

  <!-- modal menu detail tomyam -->
  <div class="modal fade" id="modal-menu8" role="dialog">
    <div class="modal-dialog modal-dialog-scrollable">
      <!-- modal content -->
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title fw-bold" id="exampleModalLabel">Detail Menu</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-sm-9">
              <p class="fw-bold pb-3 fs-5">Mae Pranom Tomyam Thai</p>
              <div class="row">
                <div class="col-8 col-sm-6 text-center">
                  <img src="assets/img/plate8.png" alt="">
                </div>
                <div class="col-4 col-sm-6 px-4">
                  <p class="fw-bold">Ingredients:</p>
                  <ul>
                    <li>coriander</li>
                    <li>lemongrass</li>
                    <li>ginger</li>
                    <li>white pepper</li>
                    <li>shrimp paste</li>
                    <li>garlic</li>
                    <li>fish sauce</li>
                  </ul>
                  <p class="fw-bold">Nutritional Info:</p>
                  <ul>
                    <li>protein <b>100g</b></li>
                    <li>carbo <b>170g</b></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer justify-content-center">
            <button type="button" class="btn btn-danger">Add to Cart</button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- modal menu detail tomyam end -->

  <!-- modal menu detail karaage -->
  <div class="modal fade" id="modal-wings" role="dialog">
    <div class="modal-dialog modal-dialog-scrollable">
      <!-- modal content -->
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title fw-bold" id="exampleModalLabel">Detail Menu</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-sm-9">
              <p class="fw-bold pb-3 fs-5">Chicken Karaage</p>
              <div class="row">
                <div class="col-8 col-sm-6 text-center">
                  <img src="assets/img/wings.png" alt="">
                </div>
                <div class="col-4 col-sm-6 px-4">
                  <p class="fw-bold">Ingredients:</p>
                  <ul>
                    <li>coriander</li>
                    <li>lemongrass</li>
                    <li>ginger</li>
                    <li>white pepper</li>
                    <li>shrimp paste</li>
                    <li>garlic</li>
                    <li>fish sauce</li>
                  </ul>
                  <p class="fw-bold">Nutritional Info:</p>
                  <ul>
                    <li>protein <b>100g</b></li>
                    <li>carbo <b>170g</b></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer justify-content-center">
            <button type="button" class="btn btn-danger">Add to Cart</button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- modal menu detail karaage end -->

  <!-- modal menu detail ramen -->
  <div class="modal fade" id="modal-plate7" role="dialog">
    <div class="modal-dialog modal-dialog-scrollable">
      <!-- modal content -->
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title fw-bold" id="exampleModalLabel">Detail Menu</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-sm-9">
              <p class="fw-bold pb-3 fs-5">Kare Burosu Ramen Wagama</p>
              <div class="row">
                <div class="col-8 col-sm-6 text-center">
                  <img src="assets/img/plate7.png" alt="">
                </div>
                <div class="col-4 col-sm-6 px-4">
                  <p class="fw-bold">Ingredients:</p>
                  <ul>
                    <li>coriander</li>
                    <li>lemongrass</li>
                    <li>ginger</li>
                    <li>white pepper</li>
                    <li>shrimp paste</li>
                    <li>garlic</li>
                    <li>fish sauce</li>
                  </ul>
                  <p class="fw-bold">Nutritional Info:</p>
                  <ul>
                    <li>protein <b>100g</b></li>
                    <li>carbo <b>170g</b></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer justify-content-center">
            <button type="button" class="btn btn-danger">Add to Cart</button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- modal menu detail ramen end -->

  <!-- modal menu detail gcurry -->
  <div class="modal fade" id="modal-plate6" role="dialog">
    <div class="modal-dialog modal-dialog-scrollable">
      <!-- modal content -->
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title fw-bold" id="exampleModalLabel">Detail Menu</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-sm-9">
              <p class="fw-bold pb-3 fs-5">Chicken Green Curry</p>
              <div class="row">
                <div class="col-8 col-sm-6 text-center">
                  <img src="assets/img/plate6.png" alt="">
                </div>
                <div class="col-4 col-sm-6 px-4">
                  <p class="fw-bold">Ingredients:</p>
                  <ul>
                    <li>coriander</li>
                    <li>lemongrass</li>
                    <li>ginger</li>
                    <li>white pepper</li>
                    <li>shrimp paste</li>
                    <li>garlic</li>
                    <li>fish sauce</li>
                  </ul>
                  <p class="fw-bold">Nutritional Info:</p>
                  <ul>
                    <li>protein <b>100g</b></li>
                    <li>carbo <b>170g</b></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer justify-content-center">
            <button type="button" class="btn btn-danger">Add to Cart</button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- modal menu detail gcurry end -->

  <!-- modal menu detail spicy asian -->
  <div class="modal fade" id="modal-plate4" role="dialog">
    <div class="modal-dialog modal-dialog-scrollable">
      <!-- modal content -->
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title fw-bold" id="exampleModalLabel">Detail Menu</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-sm-9">
              <p class="fw-bold pb-3 fs-5">Spicy Asian Cuisine</p>
              <div class="row">
                <div class="col-8 col-sm-6 text-center">
                  <img src="assets/img/plate4.png" alt="">
                </div>
                <div class="col-4 col-sm-6 px-4">
                  <p class="fw-bold">Ingredients:</p>
                  <ul>
                    <li>coriander</li>
                    <li>lemongrass</li>
                    <li>ginger</li>
                    <li>white pepper</li>
                    <li>shrimp paste</li>
                    <li>garlic</li>
                    <li>fish sauce</li>
                  </ul>
                  <p class="fw-bold">Nutritional Info:</p>
                  <ul>
                    <li>protein <b>100g</b></li>
                    <li>carbo <b>170g</b></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer justify-content-center">
            <button type="button" class="btn btn-danger">Add to Cart</button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- modal menu detail spicy asian end -->

  <!-- modal menu detail spinach salad -->
  <div class="modal fade" id="modal-plate3" role="dialog">
    <div class="modal-dialog modal-dialog-scrollable">
      <!-- modal content -->
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title fw-bold" id="exampleModalLabel">Detail Menu</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-sm-9">
              <p class="fw-bold pb-3 fs-5">Spinach Salad with Strawberry</p>
              <div class="row">
                <div class="col-8 col-sm-6 text-center">
                  <img src="assets/img/plate3.png" alt="">
                </div>
                <div class="col-4 col-sm-6 px-4">
                  <p class="fw-bold">Ingredients:</p>
                  <ul>
                    <li>coriander</li>
                    <li>lemongrass</li>
                    <li>ginger</li>
                    <li>white pepper</li>
                    <li>shrimp paste</li>
                    <li>garlic</li>
                    <li>fish sauce</li>
                  </ul>
                  <p class="fw-bold">Nutritional Info:</p>
                  <ul>
                    <li>protein <b>100g</b></li>
                    <li>carbo <b>170g</b></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer justify-content-center">
            <button type="button" class="btn btn-danger">Add to Cart</button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- modal menu detail spinach salad end -->

  <!-- modal menu crudités greek salad -->
  <div class="modal fade" id="modal-plate2" role="dialog">
    <div class="modal-dialog modal-dialog-scrollable">
      <!-- modal content -->
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title fw-bold" id="exampleModalLabel">Detail Menu</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-sm-9">
              <p class="fw-bold pb-3 fs-5">Crudités Greek Salad</p>
              <div class="row">
                <div class="col-8 col-sm-6 text-center">
                  <img src="assets/img/plate2.png" alt="">
                </div>
                <div class="col-4 col-sm-6 px-4">
                  <p class="fw-bold">Ingredients:</p>
                  <ul>
                    <li>coriander</li>
                    <li>lemongrass</li>
                    <li>ginger</li>
                    <li>white pepper</li>
                    <li>shrimp paste</li>
                    <li>garlic</li>
                    <li>fish sauce</li>
                  </ul>
                  <p class="fw-bold">Nutritional Info:</p>
                  <ul>
                    <li>protein <b>100g</b></li>
                    <li>carbo <b>170g</b></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer justify-content-center">
            <button type="button" class="btn btn-danger">Add to Cart</button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- modal menu crudités greek salad end -->

  <!-- modal menu detail cinammons -->
  <div class="modal fade" id="modal-cinammons" role="dialog">
    <div class="modal-dialog modal-dialog-scrollable">
      <!-- modal content -->
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title fw-bold" id="exampleModalLabel">Detail Menu</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-sm-9">
              <p class="fw-bold pb-3 fs-5">Cinammons Sticks</p>
              <div class="row">
                <div class="col-8 col-sm-6 text-center">
                  <img src="assets/img/cinammons-sticks 1.png" alt="">
                </div>
                <div class="col-4 col-sm-6 px-4">
                  <p class="fw-bold">Ingredients:</p>
                  <ul>
                    <li>coriander</li>
                    <li>lemongrass</li>
                    <li>ginger</li>
                    <li>white pepper</li>
                    <li>shrimp paste</li>
                    <li>garlic</li>
                    <li>fish sauce</li>
                  </ul>
                  <p class="fw-bold">Nutritional Info:</p>
                  <ul>
                    <li>protein <b>100g</b></li>
                    <li>carbo <b>170g</b></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer justify-content-center">
            <button type="button" class="btn btn-danger">Add to Cart</button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- modal menu detail cinammons end -->

  <!-- jquery -->
  <script src="node_modules/jquery/dist/jquery.slim.min.js"></script>
  <!-- bootstrap -->
  <script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
  <!-- owl carouse -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
  <script src="assets/js/script.js"></script>

  <!-- card hover -->
  <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk="
    crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"
    integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p"
    crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.min.js"
    integrity="sha384-Atwg2Pkwv9vp0ygtn1JAojH0nYbwNJLPhwyoVbhoPwBhjQPR5VtM2+xf0Uwh9KtT"
    crossorigin="anonymous"></script>
</body>

</html>