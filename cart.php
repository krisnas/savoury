<?php
include "koneksi.php";
session_start();
if (!isset($_SESSION['username'])) {
  header("Location: login.php");
}
$qcart = "select * from cart";
$data_cart = $conn->query($qcart);

?>
<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="node_modules/bootstrap/dist/css/bootstrap.min.css">

  <link rel="stylesheet" href="assets/css/style.css">
  <link rel="shorcut icon" href="assets/img/logo.png">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" />

  <title>Savoury - your needed</title>
</head>

<body>
  <nav class="navbar navbar-expand-lg navbar-light bg-light shadow-sm fixed-top">
    <div class="container-lg">
      <a class="navbar-brand" href="#">sav<span>our</span>y.</a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav ms-auto">
          <li class="nav-item active">
            <a class="nav-link" href="index.php">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#about">About</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#services">Services</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#menu">Menu</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#contact">Contact Us</a>
          </li>
          <div class="cart-icon my-auto bg-red" data-bs-toggle="modal" data-bs-target="#exampleModalToggle">
            <i class="fas fa-shopping-cart"></i>
          </div>
        </ul>
      </div>
    </div>
  </nav>
  <!-- header end -->


  <!-- home section -->
  <section class="py-5">
    <div class="container-lg">
      <div class="row justify-content-center">
        <div class="col-lg-8">
          <div class="section-title text-center">
            <h5 class=" fw-bold pt-5 text-danger">Data Order</h5>
            <h2 class="fw-bold mb-5 fs-2"><span>our</span> Amazing Customer</h2>
          </div>
        </div>
      </div>
      <table class="table table-bordered">
        <tr>
          <th>Id</th>
          <th>Menu</th>
          <th>Nama</th>
          <th>Phone Number</th>
          <th>Service</th>
          <th>Address</th>
          <th>Pay</th>
          <th>Action</th>
        </tr>

        <?php

        $sql = "SELECT id_cart, menu, nama, no_telp, layanan, alamat, bayar from cart";
        $result = $conn->query($sql);

        if ($result->num_rows > 0) {
          while ($row = $result->fetch_assoc()) {
            // echo "<tr><td>". $row["id_cart"] ."</td><td>". $row["menu"] ."</td><td>". $row["nama"] ."</td><td>". $row["no_telp"] ."</td><td>". $row["layanan"] ."</td><td>". $row["alamat"] ."</td><td>". $row["bayar"] ."</td><tr>";
            echo "<tr>";
            echo "<td>" . $row['id_cart'] . "</td>";
            echo "<td>" . $row['menu'] . "</td>";
            echo "<td>" . $row['nama'] . "</td>";
            echo "<td>" . $row['no_telp'] . "</td>";
            echo "<td>" . $row['layanan'] . "</td>";
            echo "<td>" . $row['alamat'] . "</td>";
            echo "<td>" . $row['bayar'] . "</td>";
            echo "<td>";
            // echo '<a href="update.php?id='. $row['id'] .'" class="mr-3" title="Update Record" data-toggle="tooltip"><span class="fa fa-pencil"></span></a>';
            echo '<a href="hapus_cart.php?id=' . $row['id_cart'] . '" title="Delete" data-toggle="tooltip"><span class="fa fa-trash"></span></a>';
            // echo "<a href='hapus_cart.php?id=$row[id_cart]'> Delete </a></td></tr>";


            echo "</td>";
            echo "</tr>";
          }
          echo "</table>";
        } else {
          echo "<center><b>No Data</b></center>";
        }
        ?>

    </div>
    </div>
    </div>
    </div>
    <!-- content -->

    <!-- card -->
    <div class="container mt-4">
      <div class="row">
        <div class="col-md-4">
          <div class="card mb-4 box-shadow shadow">
            <div class="card-body py-3">
              <h5 class="fw-bold">Contact Us</h5>
              <hr>
              <p class="fw-bold">Email :</p>
              <p>info@ittelkom-pwt.ac.id</p>
              <p class="fw-bold">Phone :</p>
              <p>+62-838-6384-4428</p>
              <p class="fw-bold"> Address :</p>
              <p>Jl. DI Panjaitan No.128, Karangreja, Purwokerto Kidul, Kec. Purwokerto Sel., Kabupaten Banyumas, Jawa Tengah 53147</p>
            </div>
            <div class="card-footer pt-0 border-top-0">
              <h6>Find Us on Social Media</h6>
              <hr>
              <ul class="list-unstylyed list-inline">
                <li class="list-inline-item">
                  <a href="https://www.facebook.com/ksonezz" class="btn-floating btn-lg text-primary"><i class="fab fa-facebook-f"></i></a>
                </li>
                <li class="list-inline-item">
                  <a href="https://www.instagram.com/krsnsu" class="btn-floating btn-lg text-danger"><i class="fab fa-instagram"></i></a>
                </li>
                <li class="list-inline-item">
                  <a href="https://wa.me/6281234567890""
                                    class=" btn-floating btn-lg text-success"><i class="fab fa-whatsapp"></i></a>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <!-- card end -->
        <div class="col-md-8">
          <h3 class="text-center fs-5">your Order</h3>
          <hr>
          <?php include "read_message.php" ?>
        </div>
        <form action="simpan_cart.php" method="POST">
          <div class="modal-body">
            <div class="form-floating">
              <select class="form-select" id="menu" name="menu">
                <option selected>Mae Pranom Tomyam Thai</option>
                <option>Chicken Karaage</option>
                <option>Kare Burosu Ramen Wagama</option>
                <option>Chicken Green Curry</option>
                <option>Spicy Asian Cuisine</option>
                <option>Spinach Salad with Strawberry</option>
                <option>Crudités Greek Salad</option>
                <option>Cinammons Sticks</option>
              </select>
              <label for="menu">Choose Menu</label>
            </div>
            <div class="form-floating mb-3 mt-3">
              <input type="text" class="form-control" id="nama" name="nama">
              <label for="nama">Your Name</label>
            </div>

            <div class="input-group mb-3 mt-3">
              <span class="input-group-text" id="basic-addon1">+62</span>
              <input type="text" name="no_telp" class="form-control" placeholder="Phone Number" aria-label="Username" aria-describedby="basic-addon1">
            </div>
            <div class="form-floating">
              <input class="form-control" name="layanan" id="pickdel"></input>
              <label for="layanan">Delivery / Pick Up</label>
            </div>
            <div class="mb-3 pt-3">
              <label class="form-label">Your Address</label>
              <textarea class="form-control" name="alamat" rows="3" placeholder="Your Address"></textarea>
            </div>
            <div class="mb-0">
              <label for="nama">Pay</label>
              <input type="text" class="form-control" id="bayar" name="bayar">
              <div class="col-12 text-center pt-3">
                <input type="submit" value="Kirim" class="btn btn-danger rounded">
                <span class="submitting"></span>
              </div>
        </form>
        <!-- content end-->
        </table>
      </div>
  </section>

  <!-- footer section -->
  <footer class="footer border-top py-4">
    <div class="container-lg">
      <div class="row">
        <div class="col-md-3 lg-3 col-xl-3 mx-auto mt-3">
          <h5 class="mb-4 fw-bold footer_text">sav<span>our</span>y.</h5>
          <ul class="list-unstylyed list-inline">
            <li class="list-inline-item">
              <a href="https://www.facebook.com/ksonezz" class="btn-floating btn-lg text-dark"><i class="fab fa-facebook-f"></i></a>
            </li>
            <li class="list-inline-item">
              <a href="https://www.instagram.com/krsnsu" class="btn-floating btn-lg text-dark"><i class="fab fa-instagram"></i></a>
            </li>
            <li class="list-inline-item">
              <a href="https://wa.me/6281234567890""
                class=" btn-floating btn-lg text-dark"><i class="fab fa-whatsapp"></i></a>
            </li>
          </ul>
        </div>
        <div class="col-md-2 col-lg-2 col-xl-2 mx-auto mt-3">
          <h5 class="text-uppercase mb-4 fw-bold">Services</h5>
          <p><a href="#" class="text-dark" style="text-decoration: none;">Delivery</a></p>
          <p><a href="#" class="text-dark" style="text-decoration: none;">Pricing</a></p>
          <p><a href="#" class="text-dark" style="text-decoration: none;">Fast Food</a></p>
          <p><a href="orderedmenu.php" class="text-dark" style="text-decoration: none;">Reserve your Spot</a></p>
        </div>
        <div class="col-md-3 col-lg-2 col-xl-2 mx-auto mt-3">
          <h5 class="text-uppercase mb-4 fw-bold">Information</h5>
          <p><a href="#" class="text-dark" style="text-decoration: none;">Event</a></p>
          <p><a href="#" class="text-dark" style="text-decoration: none;">Contact Us</a></p>
          <p><a href="#" class="text-dark" style="text-decoration: none;">Privacy Policy</a></p>
          <p><a href="#" class="text-dark" style="text-decoration: none;">Terms of Services</a></p>
        </div>
        <div class="col-md-4 col-lg-3 col-xl-3 mx-auto mt-3">
          <h5 class="text-uppercase mb-4 fw-bold">Address</h5>
          <p><i class="fas fa-map-marker-alt me-3"></i>Purwokerto - Jawa Tengah - Indonesia</p>
          <p><i class="fas fa-phone me-3"></i>234-234-234</p>
          <p><i class="far fa-envelope me-3"></i>savoury@mail.com</p>
        </div>
        <footer class="footer border-top py-1">
          <div class="container-lg">
            <div class="row">
              <div class="col-lg-12">
                <p class="m-0 text-center text-muted footer_text">&copy; 2021 Sav<span>our</span>y</p>
              </div>
            </div>
          </div>
        </footer>
      </div>
    </div>
  </footer>
  <!-- footer section end -->

  <script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
  <script src="node_modules/jquery/dist/jquery.slim.min.js"></script>
  <script src="node_modules/popper/popper.min.js"></script>

</body>

</html>