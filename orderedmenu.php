<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="node_modules/bootstrap/dist/css/bootstrap.min.css">
 
    <link rel="stylesheet" href="assets/css/style.css">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" />

    <title>Savoury - your needed</title>
  </head>
  <body>
  <nav class="navbar navbar-expand-lg navbar-light bg-light shadow-sm fixed-top">
    <div class="container-lg">
      <a class="navbar-brand" href="#">sav<span>our</span>y.</a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav"
        aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav ms-auto">
          <li class="nav-item active">
            <a class="nav-link" href="index.php">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#about">About</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#services">Services</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#menu">Menu</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#contact">Contact Us</a>
          </li>
          <div class="cart-icon my-auto bg-red" data-bs-toggle="modal" data-bs-target="#exampleModalToggle">
            <i class="fas fa-shopping-cart"></i>
          </div>
        </ul>
      </div>
    </div>
  </nav>
  <!-- header end -->


  <!-- home section -->
  <section class="py-5">
    <div class="container-lg">
      <div class="row justify-content-center">
        <div class="col-lg-8">
          <div class="section-title text-center">
            <h5 class=" fw-bold pt-5 text-danger">Data Reserve your Spot</h5>
            <h2 class="fw-bold mb-5 fs-2"><span>our</span> Amazing Customer</h2>
          </div>
        </div>
      </div>
      <table class="table table-bordered">
      <tr>
            <th>Id</th>
            <th>Name</th>
            <th>Phone Number</th>
            <th>Services</th>
            <th>Address</th>
            <th>Pay</th>
        </tr>
        <?php
        $conn = mysqli_connect("localhost", "root", "", "cart");
        if ($conn -> connect_error){
            die("Connection failed:".$conn -> connect_error);
        }

        $sql = "SELECT id, nama, phoneNumb, services, fulladdress, pay from ordered";
        $result = $conn -> query($sql);

        if ($result -> num_rows > 0){
            while ($row = $result -> fetch_assoc()){
                echo "<tr><td>". $row["id"] ."</td><td>". $row["nama"] ."</td><td>". $row["phoneNumb"] ."</td><td>". $row["services"] ."</td><td>". $row["fulladdress"] ."</td><td>". $row["pay"] ."</td><tr>";
            }
            echo "</table>";
        }
        else{
            echo "0 result";
        }

        $conn -> close();
        ?>
    </table>
    </div>    
  </section>

  <!-- footer section -->
  <footer class="footer border-top py-4">
    <div class="container-lg">
      <div class="row">
        <div class="col-md-3 lg-3 col-xl-3 mx-auto mt-3">
          <h5 class="mb-4 fw-bold footer_text">sav<span>our</span>y.</h5>
          <ul class="list-unstylyed list-inline">
            <li class="list-inline-item">
              <a href="https://www.facebook.com/ksonezz" class="btn-floating btn-lg text-dark"><i
                  class="fab fa-facebook-f"></i></a>
            </li>
            <li class="list-inline-item">
              <a href="https://www.instagram.com/krsnsu" class="btn-floating btn-lg text-dark"><i
                  class="fab fa-instagram"></i></a>
            </li>
            <li class="list-inline-item">
              <a href="https://wa.me/6281234567890""
                class=" btn-floating btn-lg text-dark"><i class="fab fa-whatsapp"></i></a>
            </li>
          </ul>
        </div>
        <div class="col-md-2 col-lg-2 col-xl-2 mx-auto mt-3">
          <h5 class="text-uppercase mb-4 fw-bold">Services</h5>
          <p><a href="#" class="text-dark" style="text-decoration: none;">Delivery</a></p>
          <p><a href="#" class="text-dark" style="text-decoration: none;">Pricing</a></p>
          <p><a href="#" class="text-dark" style="text-decoration: none;">Fast Food</a></p>
          <p><a href="orderedmenu.php" class="text-dark" style="text-decoration: none;">Reserve your Spot</a></p>
        </div>
        <div class="col-md-3 col-lg-2 col-xl-2 mx-auto mt-3">
          <h5 class="text-uppercase mb-4 fw-bold">Information</h5>
          <p><a href="#" class="text-dark" style="text-decoration: none;">Event</a></p>
          <p><a href="#" class="text-dark" style="text-decoration: none;">Contact Us</a></p>
          <p><a href="#" class="text-dark" style="text-decoration: none;">Privacy Policy</a></p>
          <p><a href="#" class="text-dark" style="text-decoration: none;">Terms of Services</a></p>
        </div>
        <div class="col-md-4 col-lg-3 col-xl-3 mx-auto mt-3">
          <h5 class="text-uppercase mb-4 fw-bold">Address</h5>
          <p><i class="fas fa-map-marker-alt me-3"></i>Purwokerto - Jawa Tengah - Indonesia</p>
          <p><i class="fas fa-phone me-3"></i>234-234-234</p>
          <p><i class="far fa-envelope me-3"></i>savoury@mail.com</p>
        </div>
        <footer class="footer border-top py-1">
          <div class="container-lg">
            <div class="row">
              <div class="col-lg-12">
                <p class="m-0 text-center text-muted footer_text">&copy; 2021 Sav<span>our</span>y</p>
              </div>
            </div>
          </div>
        </footer>
      </div>
    </div>
  </footer>
  <!-- footer section end -->
    
    <script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="node_modules/jquery/dist/jquery.slim.min.js"></script>
    <script src="node_modules/popper/popper.min.js"></script>
    
  </body>
</html>