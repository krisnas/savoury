$(document).ready(function () {
    $('.col-md-3').hover(
        function () {
            $(this).animate({
                marginTop: "-=.1%",
            }, 200);
        },

        function () {
            $(this).animate({
                marginTop: "0%"
            }, 200);
        }
    );
});

const sr = ScrollReveal({
    origin: 'top',
    distance: '30px',
    duration: 2000,
    reset: true
});

sr.reveal(`.home-text,
            .about-text, 
            .services, .menus, .contact, .footer`, {
    interval: 200
})

sr.reveal('.home-img', {
    duration: 2000,
    origin: 'right',
});

sr.reveal('.btn-home, .btn-about', {
    duration: 2000,
    delay: 500,
    origin: 'bottom',
});

sr.reveal('.about-img', {
    duration: 2000,
    delay: 500,
    origin: 'left',

});

sr.reveal('.showcase-kiri', {
    duration: 2000,
    delay: 500,
    origin: 'left',
    distance: '100px',
});
sr.reveal('.showcase-kanan', {
    duration: 2000,
    delay: 500,
    origin: 'right',
    distance: '100px',
});

sr.reveal('.showcase-tengah', {
    duration: 2000,
    delay: 500,
    origin: 'bottom',
    distance: '100px',
});

sr.reveal('.parator', {
    duration: 2000,
    delay: 500,
    origin: 'left',
    distance: '1500px',
});